# Pesquisas e estudos em Estrutura de Dados de Programação
## Conteúdo
###### [/tipos-dados] - Implementações sobre Tipos de Dados
###### [/matrizes] - Implementações utilizando a estrurua de dados de Matrizes/Vetores/Arrays
###### [/listas] - Implementações de estruruas de dados de Lista 
###### [/pesquisa] - Implementações de estruruas de dados de Pesquisa 
###### [/ordenacao] - Implementações de estruruas de dados de Ordenação 
## Projetos
###### [/JK] - Conteúdo dos projetos, pesquisas e trabalhos sobre estruturas de dados realizados com os alunos da Faculdade Jk de Tecnologia